# Journal de bord - Projet_PhytoPort

Projet PhytoPort lors de l'UE Méthodologie à Roscoff - Master 2 Sciences de la mer

Etude des communautés pélagiques des ports urbains anthropisés :

-  Suivi de la dynamique des communautés sur un cycle saisonnier et le long d'un gradient de confinement

Jeux de données : 
- Comptages microscopiques
- Métabarcodes (V4 18S ADNr) => Séquences et nombres d'ASV (Amplified Sequence Variants)
- Données environnementales

Hypothèses : 
- Altérnation de la saisonnalité de la prédictibilité
- Héterogénéité spatiale pour deux stations dans la composition des communautés et les réseaux d'interactions

![](Figures/Photographies-au-microscope-electronique-a-balayage-de-diatomees-epiphytes-presentes-a.png "Diatomées en rade de Brest")

_**Figure 1.** Exemples de diatomées épiphytes sur les coquilles de Crepidula fornicata présentes en rade de Brest, images de microscopie électronique à balayage._

Stratégie d'analyse proposée :
- Comparaison des jeux de données morphologiques et génétiques

1. Contribution des phylums, classes, …  aux communautés
2. Analyse plus précise pour les diatomées

- Mise en évidence de la dynamique temporelle et des différences entre stations
1. Diversité alpha et beta
2. Test des différences observées

- Construction d'un réseau de co-occurence pour explorer les relations diatomées / parasites
1. Comparer les métriques (betweeness, centrality, etc…) entre sites


## Lundi 29 Novembre 2021
TP Initiation à GitLab

Création du projet sur GitLab

Mise en place des dossiers et du fonctionnement sur GitLab

## Mardi 30 Novembre

Récupération des jeux de données
- Comptage microscopique
- ASV issus de métabarcoding

Articles mis à notre disposition :
1. Berry, D., Widder, S., 2014. Deciphering microbial interactions and detecting keystone species with co-occurrence networks. Frontiers in Microbiology 5, 219. https://doi.org/10.3389/fmicb.2014.00219
2. Faust, K., Raes, J., 2012. Microbial interactions: from networks to models. Nat Rev Microbiol 10, 538–550. https://doi.org/10.1038/nrmicro2832
3. Ishimoto, C.K., Aono, A.H., Nagai, J.S., Sousa, H., Miranda, A.R.L., Melo, V.M.M., Mendes, L.W., Araujo, F.F., de Melo, W.J., Kuroshu, R.M., Esposito, E., Araujo, A.S.F., 2021. Microbial co-occurrence network and its key microorganisms in soil with permanent application of composted tannery sludge. Science of The Total Environment 789, 147945. https://doi.org/10.1016/j.scitotenv.2021.147945
4. Kenworthy, J.M., Rolland, G., Samadi, S., Lejeusne, C., 2018. Local variation within marinas: Effects of pollutants and implications for invasive species. Marine Pollution Bulletin 133, 96–106. https://doi.org/10.1016/j.marpolbul.2018.05.001
5. Lambert, S., Tragin, M., Lozano, J.-C., Ghiglione, J.-F., Vaulot, D., Bouget, F.-Y., Galand, P.E., 2019. Rhythmicity of coastal marine picoeukaryotes, bacteria and archaea despite irregular environmental perturbations. ISME J 13, 388–401. https://doi.org/10.1038/s41396-018-0281-z
6. Ma, B., Wang, H., Dsouza, M., Lou, J., He, Y., Dai, Z., Brookes, P.C., Xu, J., Gilbert, J.A., 2016. Geographic patterns of co-occurrence network topological features for soil microbiota at continental scale in eastern China. ISME J 10, 1891–1901. https://doi.org/10.1038/ismej.2015.261
7. Shaikh, S.M.S., Tagde, J.P., Singh, P.R., Dutta, S., Sangolkar, L.N., Kumar, M.S., 2021. Impact of Port and harbour activities on plankton distribution and dynamics: A multivariate approach. Marine Pollution Bulletin 165, 112105. https://doi.org/10.1016/j.marpolbul.2021.112105

### Matin
Observations des jeux de données sur Excel
   - Harmonisation de la nomenclature des noms d'espèces (remplacements des espaces par des underscores, suppression des points, création d'une colonne "Genus", pour le genre, et "Species", pour le genre + espèce)

   - Determiner les intercepts entre les deux jeux de données: microscope et métabarcoding sur R et excel (double vérification)

--> 29 espèces en commun entre les deux (33 genres)

Début de la réflexion sur l'acquisition des données

### Après-midi 
- Compléter les catégories taxonomiques du fichier microscopie pour avoir la même resolution que le fichier métabarcoding avec l'outil automatique de Worms et manuellement à partir de Worms 

- Etablissement d'un dendrogramme phylogénétique jusqu'à l'espèce, colorée suivant la présence de l'espèce dans l'un, l'autre ou les deux jeux de données

![](Figures/Rplot04.png "Arbre basé sur la classification taxonomique")

_**Figure 2.** Arbre obtenu à partir de la classification taxonomique, orange = microscope ; blue = métabarcoding ; mauve = microscopie._

Code associé :

```
library(ape)
library(readr)
library("dendextend")
library(dplyr)

tree <- read_delim("tree.csv", delim = ";", 
                   escape_double = FALSE, trim_ws = TRUE)
View(tree)
tree$Phylum <- as.factor(tree$Phylum)
tree$Class <- as.factor(tree$Class)
tree$Order <- as.factor(tree$Order)
tree$Family <- as.factor(tree$Family)
tree$Genus <- as.factor(tree$Genus)
tree$Species <- as.factor(tree$Species)
tree <- tree %>%
  mutate(colour = ifelse(tree$Type == "mic", "blue",
                ifelse(tree$Type == "met", "green", "red")))

frm <- ~Phylum/Class/Order/Family/Genus/Species
tr <- as.phylo(frm, data = tree, collapse = FALSE)
tr$edge.length <- rep(1, nrow(tr$edge))

etiquette <- data.frame(tr$tip.label)
write.table(etiquette, "etiquette.csv", row.names=FALSE, sep=";",dec=",", na=" ")
etiquette <- read_delim("etiquet.csv", 
                        delim = ";", escape_double = FALSE, trim_ws = TRUE)

plot(tr, show.node.label = FALSE, type = "fan",
     cex = 0.5, tip.col = etiquette$Colour, underscore = FALSE)
legend(7, 9, legend=c("Microscopy", "Metabarcoding", "Both"),
       fill = c("orange", "turquoise3", "darkorchid4"), box.lty=0 , cex=0.8)
```

- Mise en forme du fichier Excel métabarcoding pour avoir des abondances relatives

- Préaparation du schéma d'acquisition des données après relecture par N. Simon et B. Alric

![](Figures/Acquisition.png "Processus d'acquisition des données")

_**Figure 3.** Processus d'acquisition des données depuis l'échantillonnage jusqu'aux jeux de données finaux._

- Lecture d'articles (4, 5 et 7 de la liste précédente) et prise de notes sur Google Doc

- A continuer
   - [x] Continuer les indices de diversités
   - [ ] Lire les articles pour idée de l'introduction
   - [ ] Essayer d'améliorer la figure de l'Arbre
   - [x] Se renseigner sur les réseaux de co-occurrences (lire les articles)


## Mercredi 01 Décembre

### Matin

- Visite de la plateforme métagénomique (nos ADN ont été récupérées via une extraction liquide-liquide - car bonne reproductibilité, lors d'une analyse ciblée sur la partie v4 du gène 18S)

- Diversité alpha (Piélou, Shannon et richesse spécifique par station au cours du temps) (voir script "Tuesday")

![](Figures/Capture_d_écran_2021-12-01_à_17.31.42.png "Graphes de diversité")

_**Figure 4.** Diversité alpha dans les stations H42 et B8 au cours du temps._

- Essai d'amélioration de la Figure 2 (une couleur par phylum par exemple) mais toujours en cours

### Après-midi 

- Calculs des distances (euclidiennes, bray-curtis et khi²) des abondances relatives entre station pour toutes les diatomées (Script "Tuesday")

- Clustering sur les communautés pour chaque date (Script "Tuesday")
   --> Les communautés se regroupent par paire (2 stations) suivant la date d'échantillonnage, le facteur temporel semble plus important que la différence de communautés entre les stations

- NMDS sur les communautés par station et date (graphique à améliorer, voir script "Tuesday" d'Hannah)

- Suivi du script "Cooccurrence_network_UE_projet_2021" donné par B. Alric afin de construire les réseaux de co-occurrences (mais problème car aucune interaction à la fin)

- A continuer
   - [ ] Essayer d'améliorer la figure de l'Arbre
   - [ ] Faire une belle figure pour le NMDS
   - [ ] Finir le réseau de co-occurrence
   - [ ] Commencer la rédaction


Point avec B. Alric :
- Aide sur les réseaux de co-occurrences
- Ajout d'un article (8.) Scholz, B., Guillou, L., Marano, A.V., Neuhauser, S., Sullivan, B.K., Karsten, U., Küpper, F.C., Gleason, F.H., 2016. Zoosporic parasites infecting marine diatoms — A black box that needs to be opened. Fungal Ecol 19, 59–76. https://doi.org/10.1016/j.funeco.2015.09.002

   A faire :
      - [x] Faire 2 réseaux (un par site)
      - [x] Les colorer suivant le type d'organismes
      - [x] Chercher des métriques pour comparer les réseaux
      - [ ] Etudier les co-occurences de nos réseaux et les comparer à la littérature
      - [ ] Faire des tests statistiques sur les groupes (H42 et B8 ou par saison) faits _a priori_ sur le NMDS des communautés par station et date

Fin du point

- Réalisation d'un réseau par station et figures préliminaires (Script "Cooccurrences")

![](Figures/B8_network.png "Réseau B8")

_**Figure 5.** Réseau des co-occurrences à la station B8._

![](Figures/H42_network.png "Réseau H42")

_**Figure 6.** Réseau des co-occurrences à la station H42._


## Jeudi 02 Décembre

### Matin

- Profinement des graphiques de NMDS, encerclement selon station et saison
- Nouveau script pour plotter les NMDS (geom_polygon) donné par Benjamin (voir script "Thursday")
- Création des slides pour la présentation des figures (https://docs.google.com/presentation/d/1G6GfFcqc72HfJ4M5VVAqdU3oYLgAnfaO0s2TxKZTGPI/edit#slide=id.g103cc74518d_0_154)

- Récupération des co-occurrences depuis les 2 réseaux et identifications des associations (voir script 0212_cooccurrences.R et dataset edges_B8 et edges_H42)
- Réseaux de co-occurrences en utilisant des surnoms pour les noms d'espèces (voir correspondances dans le dataset Noms_abbreviations.xlsx)

### Après-midi

- Presentation des figures et retour
   - Faire des légendes complètes
   - Simplifier les figures sur la diversité alpha et améliorer celle sur le NMDS
   - Choisir quelles figures dans le rapport
- Correction des graphiques de diversité alpha pour obtenir un graph avec les différences d'indices entre les 2 stations
- Effectuer un test de Perm-anova sur les coordonnées du NMDS pour pouvoir dire que la saisonalité à un effet significative sur la diversité (présence/absence des espèces) et non les stations

- Rédaction de l'introduction
- Choix des figures à présenter
- Modification de la figure de l'arbre car mauvaise annotation de certains groupes (e.g. Chaetoceros) lors du complément par Worms
- Comparaison des co-occurrences obtenues avec celles de l'article 8 et de la base de données Aquasymbio (http://www.aquasymbio.fr/fr)


A faire :
   - [ ] Finir les figures
   - [ ] Rédaction de la partie résultats
   - [ ] Préparation de l'oral + slides

## Vendredi 03 Décembre

### Matin

- Mise en forme de figures pour l'oral et le rapport (voir figures suivantes)

![](Figures/Arbre.jpg "Arbre taxonomique")

_**Figure 7.** Arbre simple._

![](Figures/Arbre+nom.jpg "Arbre taxonomique avec groupes identifiées")

_**Figure 8.** Arbre simple avec les groupes biaisés par l'observation microscopique._

![](Figures/Arbre+diatomparasit.jpg "Arbre taxonomique avec diatomées et groupes de parasites")

_**Figure 9.** Arbre avec les groupes de diatomées et parasites identifiés._

![](Figures/Diagramme_venn.png "Diagramme de Venn des 2 réseaux")

_**Figure 10.** Diagramme de Venn des 2 réseaux._

- Terminer la présentation orale avec quelques répétitions
- Redaction de l'introduction et sélection des figures pour le rapport 


### Après-midi

- Oral à 13h30

A faire :
   - [ ] Finir la rédaction du rapport






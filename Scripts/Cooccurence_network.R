# ================================================ #
# Script R - UE projet 2021 - Marina port de Brest #
# ================================================ #


# ============================================================================================================= #
#                                                                                                               #
# Objectif:                                                                                                     #
# Construction des r?seaux de co-occurrence                                                                     #
# Caract?risation de la structure des r?seaux de co-occurrence au moyen de m?triques de topolgie des r?seaux    #
# Donn?es:                                                                                                      #
# Pour cette partie du projet utiliser uniquement les donn?es de m?tabarcodes (18S V4)                                   #
# M?thodes:                                                                                                     #
# Inf?rer les r?seaux de co-occurrence ? partir d'une m?thode bas?e sur de la corr?lation                       #
#                                                                                                               #
# ============================================================================================================= #


#---------------------------------------------------------------------#
## 01. Packages n?cessaires aux analyses
library(igraph)
library(ggplot2)
library(Hmisc)


#---------------------------------------------------------------------#
## 02. S?lectionner les ASVs affili?s aux diatom?es (h?tes) et ? leurs parasites associ?s
##     Ceci pour chaque site (i.e. B8 et H42)

## Diatom?es
## Q1: Quelle classe doit-on utiliser pour s?lectionner uniquement les diatom?es dans la table "metab_18S_3?m_UE_projet_2021.csv"
#bacillariophyta
## Parasites
## Q2: Vous vous focaliserez sur la division des "Cercozoa" et la classe des "Labyrinthulomycetes", la classe des "Chytridiomycota", 

setwd("~/Desktop/M2/_UE_Projet_Roscoff/R")
#---------------------------------------------------------------------#
## 03. Estimer les associations ? l'?chelle du groupe h?te-parasite
## 03.1. Transformer la table filtr?e en table binaire (0/1)
library(readxl)
diap <- read_excel("diatom-parasite.xlsx", 
                   col_types = c("skip", "skip", "skip", 
                                 "skip", "text", "skip", "numeric", 
                                 "numeric", "numeric", "numeric", 
                                 "numeric", "numeric", "numeric", 
                                 "numeric", "numeric", "numeric", 
                                 "numeric", "numeric", "numeric", 
                                 "numeric", "numeric", "numeric", 
                                 "numeric", "numeric", "numeric", 
                                 "numeric", "numeric", "numeric", 
                                 "numeric", "numeric", "numeric", 
                                 "numeric"))
View(diap)
diap2 <- data.frame(t(diap[-1]))

colnames(diap2) <- unique(diap$Species)
View(diap2)

diap2[diap2 > 0] <- 1
View(diap2)


## 03.2. Estimer les associations par paires en calculant une matrice de corr?lation (utiliser le coefficient de corr?lation de spearman)

cor_diap <- cor(diap2, method="spearman")
View(cor_diap)



## 03.3.Estimer les p-values associ?es ? chaque valeur de corr?lation.
##      Pour réaliser ceci, chercher une fonction (associ?e ? un pakcage R) vous
# permettant d'estimer d'une part les coefficients de corr?lations et d'autre part
# les p-value associ?es
##Lorsque vous réalisez des test multiples comme c'est le cas ici,
# pouvez-vous utiliser les p-values comme telles?

pval_cor <- rcorr(cor_diap, type="spearman")
View(pval_cor$P)



length(pval_cor$P[pval_cor$P>0.05])/length(pval_cor$P)

### 27% des p-value des test de correlation ne sont pas significative !


## 03.4. Une fois la matrice de corr?lation obtenue, consid?rer uniquement les corr?lations positives
##       S?lectionner les corr?lations ? garder selon deux seuils:
##       - rho > 0.6 (si rho <= 0.6 mettre 0)
##       - p-value <= 0.05 (si p-value > 0.05 mettre 0)

cor_diap_rho <- cor_diap
cor_diap_rho[cor_diap_rho<=0.6 | pval_cor$P>0.05] <- 0



View(cor_diap_rho)


## 03.5. Nous y sommes presque, nous pouvons maintenant visualiser les r?seaux de co-occurrence
##       Pour cela utiliser le package "igraph"
## 03.5.1. La premi?re ?tape est de transformer la matrice de corr?lation en un objett igraph
##         Pour cela utiliser la fonction graph.adjency()

reseau <- graph.adjacency(cor_diap_rho)
plot(reseau)



## 03.5.2. Supprimer toutes les associations en boucle (entre un m?me h?te ou un m?me parasite)
##         Pour cela utiliser la fonction simplify()


plot(simplify(reseau, remove.multiple = T))

#   # 03.5.3. Visualiser les r?seaux de co-occurrence
##         Pour cela utiliser la fonction plot()
##         Pr?ciser l'argument layout avec "layout_in_circle() pour avoir
#un r?seau repr?sent? en cercle
##         Vous pouver modifier d'autre argument dans la fonction plot() pour 
# colorer les noeuds du r?seaux selon les groupes (e.g. h?tes, parasites) 

plot(layout_in_circle(simplify(reseau, remove.multiple = T)))





## 03.5.4. D'apr?s les articles sur les r?seaux de co-occurrence quelles m?triques pourraient ?tre utilis?es pour caract?riser la structure des r?seaux ?
##         Une fois ces m?triques (la m?thode) identifi?es, faita l'analyse pour caract?riser la structure des r?seaux


setwd("~/Desktop/M2/_UE_Projet_Roscoff/R")

library(scales)
show_col(hue_pal()(2))
## data sets

library(readr)

micro <- read_delim("Modif_sheets/micro_spelling.csv", 
                    delim = ";", escape_double = FALSE, trim_ws = TRUE)
View(micro)

meta <- read_delim("meta-sp-g.csv", delim = ";", 
                   escape_double = FALSE, trim_ws = TRUE)
View(meta)


## comparing two lists of categorical variables

mic = micro$Species
met = meta$Species

mic_g = micro$Genus
met_g = meta$Genus

mic %in% met 

intersect(mic,met)
length(intersect(mic,met))
length(mic)
setdiff(mic,met)

intersect(mic_g,met_g)
length(intersect(mic_g,met_g))
length(unique(mic_g))
length(unique(met_g))

length(unique(met))
length(unique(mic))

table(mic,)
##### Exemple internet ####
### ex
sample <- paste(rep("sample_",24) , seq(1,24) , sep="")
specie <- c(rep("dicoccoides" , 8) , rep("dicoccum" , 8) , rep("durum" , 8))
treatment <- rep(c(rep("High",4 ) , rep("Low",4)),3)
data <- data.frame(sample,specie,treatment)
View(data)
for (i in seq(1:5)){
  gene=sample(c(1:40) , 24 )
  data=cbind(data , gene)
  colnames(data)[ncol(data)]=paste("gene_",i,sep="")
}
data[data$treatment=="High" , c(4:8)]=data[data$treatment=="High" , c(4:8)]+100
data[data$specie=="durum" , c(4:8)]=data[data$specie=="durum" , c(4:8)]-30
rownames(data) <- data[,1] 



frm <- ~SuperFamily/Family/Genus/Species
tr <- as.phylo(frm, data = carnivora, collapse=FALSE)
tr$edge.length <- rep(1, nrow(tr$edge))
plot(tr, show.node.label=TRUE, cex=0.5)
Nnode(tr)

## compare with:
Nnode(as.phylo(frm, data = carnivora, collapse = FALSE))


df <- data.frame(micro = c(rep(1, 4), rep(0, 4), rep(1, 2)), metab = c(rep(0, 4), rep(1, 4), rep(1, 2)), both = c(rep(0, 8), rep(1, 2)))

hc1 <- hclust(dist(df))

plot(hc1)

## with our data

frm <- ~SuperFamily/Family/Genus/Species
tr <- as.phylo(frm, data = carnivora, collapse=FALSE)
tr$edge.length <- rep(1, nrow(tr$edge))
plot(tr, show.node.label=TRUE, cex=0.5)
Nnode(tr)



##### diversity ####

setwd("~/Desktop/M2/_UE_Projet_Roscoff/R/Modif_sheets")


library(readxl)
meta_diversity <- read_excel("meta_diversity.xlsx", 
                             col_types = c("skip", "skip", "skip", 
                                           "skip", "text", "skip", "numeric", 
                                           "numeric", "numeric", "numeric", 
                                           "numeric", "numeric", "numeric", 
                                           "numeric", "numeric", "numeric", 
                                           "numeric", "numeric", "numeric", 
                                           "numeric", "numeric", "numeric", 
                                           "numeric", "numeric", "numeric", 
                                           "numeric", "numeric", "numeric", 
                                           "numeric", "numeric", "numeric", 
                                           "numeric"))
View(meta_diversity)

## remove the species names for diversity

meta_shannon <- meta_diversity[,-1]
library(vegan)
## write data frame shan with shannon idex per station/date
shan <-as.data.frame(diversity(meta_shannon, index = "shannon", MARGIN = 2, base = exp(1)))
colnames(shan)<- "shannon"
## add eveness
shan$eveness <-shan$shannon/log2(specnumber(meta_shannon, MARGIN = 2))
#and richness
shan$richness <- specnumber(meta_shannon, MARGIN=2)
# add rownames to dataframe

library(tidyr)
shan$stations <- row.names(shan)

# separate to get station and date
shan <-separate(shan,stations,c("station","date"), "_")
View(shan)


## plotting with ggplot

A <-ggplot(shan, aes(date, eveness, color = station, group=station)) +
  geom_point(aes(size =2))+
  geom_line()+
  labs(x="Date", y="Pielou's eveness index", color="Stations")+
  theme(axis.text=element_text(size=14),
        axis.title=element_text(size=14,face="bold"),
        legend.text=element_text(size=12),
        legend.key.size = unit(2,"line"),
        axis.text.x = element_text(angle = 45, vjust = 0.5, hjust=1))+
  ggtitle("Evenness")

B <-ggplot(shan, aes(date, shannon, color = station, group=station)) +
  geom_point(aes(size =2))+
  geom_line()+
  labs(x="Date", y="Pielou's eveness index", color="Stations")+
  theme(axis.text=element_text(size=14),
        axis.title=element_text(size=14,face="bold"),
        legend.text=element_text(size=12),
        legend.key.size = unit(2,"line"),
        axis.text.x = element_text(angle = 45, vjust = 0.5, hjust=1))+
  ggtitle("Shannon")


C <-ggplot(shan, aes(date, richness, color = station, group=station)) +
  geom_point(aes(size =2))+
  geom_line()+
  labs(x="Date", y="Pielou's eveness index", color="Stations")+
  theme(axis.text=element_text(size=14),
        axis.title=element_text(size=14,face="bold"),
        legend.text=element_text(size=12),
        legend.key.size = unit(2,"line"),
        axis.text.x = element_text(angle = 45, vjust = 0.5, hjust=1))+
  ggtitle("Species richness")


pdf(file = "~/Desktop/M2/_UE_Projet_Roscoff/figures/diversity",   
    width = 20, 
    height = 15) 

grid.arrange(A, B,C, nrow=2)

dev.off()
### making dates more agreeable

shan$month <-rep(c("march_19","april_19","may_19","june_19","july_19","august_19","sept_19","oct_19","nov_19","dec_19","jan_20","feb_20","march_20"),2)
month_order <-c("march_19","april_19","may_19","june_19","july_19","august_19","sept_19","oct_19","nov_19","dec_19","jan_20","feb_20","march_20")


A2 <-ggplot(shan, aes(factor(month, level = month_order), eveness, color = station,group=station)) +
  geom_point(aes(size =2))+
  geom_line()+
  labs(x="Date", y="Pielou's eveness index", color="Stations")+
  guides(size="none", color="none")+
  theme(axis.text=element_text(size=14),
        axis.title=element_text(size=14,face="bold"),
        title=element_text(size=16,face="bold"),
        legend.text=element_text(size=12),
        legend.key.size = unit(2,"line"),
        axis.text.x = element_text(angle = 45, vjust = 0.8, hjust=1),
        plot.title = element_text(vjust = - 10, hjust=0.01))+
  ggtitle("A")

B2 <-ggplot(shan, aes(factor(month, level = month_order), shannon, color = station,group=station)) +
  geom_point(aes(size =2))+
  geom_line()+
  labs(x="Date", y="Shannon's diversity index", color="Stations")+
  guides(size="none", color="none")+
  theme(axis.text=element_text(size=14),
        axis.title=element_text(size=14,face="bold"),
        title=element_text(size=16,face="bold"),
        legend.text=element_text(size=12),
        legend.key.size = unit(2,"line"),
        axis.text.x = element_text(angle = 45, vjust = 0.8, hjust=1),
        plot.title = element_text(vjust = - 10, hjust=0.01))+
  ggtitle("B")

B2

C2 <-ggplot(shan, aes(factor(month, level = month_order), richness, color = station,group=station)) +
  geom_point(aes(size =2))+
  geom_line()+
  labs(x="Date", y="Number of species", color="Stations")+
  guides(size="none")+
  theme(axis.text=element_text(size=14),
        axis.title=element_text(size=14,face="bold"),
        title=element_text(size=16,face="bold"),
        legend.text=element_text(size=12),
        legend.key.size = unit(2,"line"),
        axis.text.x = element_text(angle = 45, vjust = 0.8, hjust=1),
        plot.title = element_text(vjust = - 10, hjust=0.01), 
        legend.position = c(0.8, 0.2))+
  ggtitle("C")

C2
### combine two graphs : Not good enough apparently
coef = 600

ggplot(shan, aes(x=factor(month, level = month_order))) +
  geom_point(aes(y= eveness, color = station,group=station))+
  geom_line(aes( y=eveness, color = station, group=station))+
  geom_histogram(aes(y= richness/coef, fill = station, group=station ), colour="black", stat = "identity", position="dodge")+
  labs(x="Date", color="Stations")+
  guides(size="none", color="none")+
  theme(axis.text=element_text(size=14),
        axis.title=element_text(size=14,face="bold"),
        title=element_text(size=16,face="bold"),
        legend.text=element_text(size=12),
        legend.key.size = unit(2,"line"),
        axis.text.x = element_text(angle = 45, vjust = 0.8, hjust=1),
        plot.title = element_text(vjust = - 10, hjust=0.01))+
    scale_y_continuous(name="Pielou's diversity index",sec.axis = sec_axis(~.*coef, name="Number of species"))+
  ggtitle("A")

  
  ## histogram with richness 
ggplot(shan) +
  geom_bar(aes(size =2, x=factor(month, level = month_order), y=richness, fill = station, group=station ),stat="identity",position = "dodge")

pil_dif <- as.data.frame(shan$eveness[shan$station=="B8"]-shan$eveness[shan$station=="H42"])
View(pil_dif)
colnames(pil_dif) <- "Eveness"
#pil_dif$shannon <-shan$shannon[shan$station=="B8"]-shan$shannon[shan$station=="H42"]

pil_dif$richness <-shan$richness[shan$station=="B8"]-shan$richness[shan$station=="H42"]
pil_dif$month <- month_order


ggplot(pil_dif, aes(x=factor(month, level = month_order))) +
  geom_histogram(aes(y= richness), colour="black", stat = "identity", position="dodge")+
  labs(x="Date")+
  guides(size="none", color="none")+
  theme(axis.text=element_text(size=14),
        axis.title=element_text(size=14,face="bold"),
        title=element_text(size=16,face="bold"),
        legend.text=element_text(size=12),
        legend.key.size = unit(2,"line"),
        axis.text.x = element_text(angle = 45, vjust = 0.8, hjust=1),
        plot.title = element_text(vjust = - 10, hjust=0.01))

pil_dif_2 <-pil_dif
View(pil_dif_2)

pil_dif_2$Richness <-pil_dif_2$richness/200
pil_dif_2 <-pil_dif_2[-2]
library(ggplot2)

pdf(file = "~/Desktop/M2/_UE_Projet_Roscoff/figures/diversity_beta",   
    width = 11, 
    height = 7) 

pil_dif_2 %>% 
  pivot_longer(-month, names_to = "Indexes", values_to = "value") %>% 
  ggplot(aes(x=factor(month, level = month_order), y = value, fill = Indexes)) + 
  geom_bar(stat = "identity", position = "dodge", colour="black")+
  scale_y_continuous(name="Pielou's diversity index",sec.axis = sec_axis(~.*200, name="Richness"))+
  theme(legend.position = "bottom")+
  labs(x="Date")+
  scale_fill_manual(values=c("#999999", "#E69F00"))

dev.off()
### the axis are wrong ! 

coef= 30
ggplot(pil_dif, aes(x=factor(month, level = month_order))) +
  geom_histogram(aes(y= Eveness, fill="blue"), colour="black", stat = "identity", position="dodge")+
  geom_histogram(aes(y= richness/coef, fill="orange"), colour="black", stat = "identity", position="dodge")+
  labs(x="Date")+
  guides(size="none", color="none")+
  theme(axis.text=element_text(size=14),
        axis.title=element_text(size=14,face="bold"),
        title=element_text(size=16,face="bold"),
        legend.text=element_text(size=12),
        legend.key.size = unit(2,"line"),
        axis.text.x = element_text(angle = 45, vjust = 0.8, hjust=1),
        plot.title = element_text(vjust = - 10, hjust=0.01))+
  scale_y_continuous(name="Pielou's diversity index",sec.axis = sec_axis(~.*coef, name="Number of species"))


pdf(file = "~/Desktop/M2/_UE_Projet_Roscoff/figures/diversity_month_2",   
    width = 30, 
    height = 7) 

grid.arrange(A2, B2,C2, nrow=1)

dev.off()

#### beta diversity ####

## mise en forme de dataset
beta <- data.frame(t(meta_diversity[-1]))
colnames(beta) <- unique(meta_diversity$Species)
View(beta)

## convert into binary data frame presence-absence !
beta_bi <- beta
beta_bi[beta_bi > 0] <- 1
View(beta_bi)

#### with betapart package ?####

beta_div <-betapart.core(beta_bi)
View(beta_div)


## pairwise with rdist ?
B8_beta <- beta[1:13,]
View(B8_beta)

H42_beta <- beta[14:26,]
View(H42_beta)

eu_bet <-rdist(B8_beta, H42_beta)


## distance euclideen

diversity_euclid <- dist(beta, method="euclidian")
diversity_euclid

### with vegan 

ncol(meta_shannon)/mean(specnumber(meta_shannon)) - 1




##### nmds --> code de AMEM ####
library(vegan)
beta_Dist <- vegdist(beta, method="bray")
class(beta_Dist)
# compute NMDS
beta_NMDS <- metaMDS(beta_Dist, k=2) # keep 3 dimensions
plot(beta_NMDS$points)
str(beta_NMDS)
beta_NMDS$stress

## script de seb

data_scoresBC=as.data.frame(scores(beta_NMDS))
data_scoresBC$date <- rownames(beta)

library(tidyr)
data_scoresBC<-separate(data_scoresBC,date,c("station","date"), "_")
View(data_scoresBC)
data_scoresBC$month <-rep(c("march_19","april_19","may_19","june_19","july_19","august_19","sept_19","oct_19","nov_19","dec_19","jan_20","feb_20","march_20"),2)

library(ggplot2)
library(ggrepel)
library(ggalt)

data_B8 <- subset(data_scoresBC, station == "B8")
data_H42 <- subset(data_scoresBC, station == "H42")

pdf(file = "~/Desktop/M2/_UE_Projet_Roscoff/figures/NMDS_stations",   
    width = 10, 
    height = 7) 

ggplot(data_scoresBC,aes(x=NMDS1,NMDS2,label=month, color=station))+
  geom_point()+
  geom_encircle(aes(x=NMDS1, y=NMDS2), 
                data=data_B8, 
                color="#F8766D", 
                size=1, 
                expand=0.01) +   
  geom_encircle(aes(x=NMDS1, y=NMDS2), 
                data=data_H42, 
                color="#00BFC4", 
                size=1, 
                expand=0.01) +  
  geom_text_repel(label=data_scoresBC$month)+
  xlab("NMDS1")+
  ylab("NMDS2")+
  labs(title="NMDS with Bray-Curtis coefficient (stress =0.09)")

dev.off()

data_scoresBC$season <-rep(c("Spring","Spring","Spring","Spring","Summer","Summer","Summer","Winter","Winter","Winter","Winter","Winter","Spring"),2)

data_spring <- subset(data_scoresBC, season == "Spring")
data_summer <- subset(data_scoresBC, season == "Summer")
data_winter <- subset(data_scoresBC, season == "Winter")



pdf(file = "~/Desktop/M2/_UE_Projet_Roscoff/figures/NMDS_seasons",   
    width = 10, 
    height = 7) 


ggplot(data_scoresBC,aes(x=NMDS1,NMDS2,label=month, color=station))+
  geom_point()+
  geom_encircle(aes(x=NMDS1, y=NMDS2), 
                data=data_spring, 
                color="orange", 
                size=1, 
                expand=0.01) +   
  geom_encircle(aes(x=NMDS1, y=NMDS2), 
                data=data_summer, 
                color="red", 
                size=1, 
                expand=0.01) + 
  geom_encircle(aes(x=NMDS1, y=NMDS2), 
                data=data_winter, 
                color="blue", 
                size=1, 
                expand=0.01) + 
  annotate("text", x=0.27, y= 0.125,label= "Summer", color="red", fontface =2)+
annotate("text", x=0.2, y= -0.25,label= "Spring", color="orange", fontface =2)+
  annotate("text", x=-0.1, y= 0.06, label="Winter", color="blue", fontface =2)+
  geom_text_repel(label=data_scoresBC$month)+
  xlab("NMDS1")+
  ylab("NMDS2")+
  labs(title="NMDS with Bray-Curtis coefficient (stress =0.09)")


dev.off()

## Create convex hull for each season
hull_nmds <- data_scoresBC %>%
  group_by(season) %>%
  slice(chull(NMDS1, NMDS2))
pdf(file = "~/Desktop/M2/_UE_Projet_Roscoff/figures/NMDS_New_seasons2",   
    width = 10, 
    height = 7) 


## Visualization of beta-diversity
data_scoresBC %>%
  ggplot(aes(x = NMDS1, y = NMDS2)) +
  geom_point(shape = factor(data_scoresBC$station)) +
  aes(fill = factor(season)) +
  geom_text_repel(label=data_scoresBC$month)+
  geom_polygon(data = hull_nmds, alpha = 0.5)+
  labs(fill='Season') 



## par station
hull_nmds_station <- data_scoresBC %>%
  group_by(station) %>%
  slice(chull(NMDS1, NMDS2))

pdf(file = "~/Desktop/M2/_UE_Projet_Roscoff/figures/NMDS_New_seasons",   
    width = 10, 
    height = 7) 

## Visualization of beta-diversity
data_scoresBC %>%
  ggplot(aes(x = NMDS1, y = NMDS2,label=month)) +
  geom_point(shape = 21) +
  aes(fill = factor(station)) +
  geom_text_repel(label=data_scoresBC$month)+
  geom_polygon(data = hull_nmds_station, alpha = 0.5)+
  labs(fill='Station') 

dev.off()



### perm ANOVA

## avec donnée binaire
meta_long <- meta_diversity %>%
  pivot_longer(cols=!Species, names_to = "Station_date", values_to = "relative")
View(meta_long)
meta_long_bi <-meta_long
meta_long_bi <- meta_long_bi[,3]
View(meta_long_bi)
meta_long_bi[meta_long_bi > 0] <- 1

View(meta_long_bi)
meta_long_bi$Species <- meta_long$Species
meta_long_bi$Station_date <- meta_long$Station_date
View(meta_long_bi)

meta_long_bi <-separate(meta_long_bi,Station_date,c("station","date"), "_")
View(meta_long)

library(RVAideMemoire)


perm.anova(relative ~ station * date, nest.f2 = c("fixed", "random"),
           meta_long_bi, nperm = 999,progress = TRUE)



## avec donnée de distances de bray-curtis

library(vegan)
beta_Dist <- vegdist(beta, method="bray")
beta_Dist

library(reshape2)

dist_anova <- melt(as.matrix(beta_Dist), varnames = c("row", "col"))
View(dist_anova)


### so this is wrong not suppose to use bray-curtis directly rather use the NMDS coordiantes

# use this 
#data_scoresBC
beta_NMDS <- metaMDS(beta_Dist, k=2) # keep 3 dimensions
plot(beta_NMDS$points)
str(beta_NMDS)
beta_NMDS$stress

## script de seb

data_scoresBC=as.data.frame(scores(beta_NMDS))
data_scoresBC$date <- rownames(beta)


data.matrix(vegdist(step4,method="bray"))

please <- adonis (data_scoresBC[,1:2] ~station + season, method = "euclidean", data= data_scoresBC)

please


install.packages('devtools')
library(devtools)
install_github("pmartinezarbizu/pairwiseAdonis/pairwiseAdonis") library(pairwise.adonis)


pair.mod<-pairwise.adonis(data_scoresBC[,1:2],factors=data_scoresBC$season)
pair.mod



#### script from AMEM ####
# define color based on gender
library("stringr")
station <- str_sub(row.names(beta), 1,1)  # takes the first letter
cols <- ifelse(station == "B", "#2E3B99", "#AA3D63")

# plot those
library("rgl")
# prepare window
plot3d(coords, type="n")
# add text
text3d(coords, texts=row.names(coords), col=cols)
par3d(windowRect=c(20,20,600,600))



## clustering ####

tree_euclid_single <- hclust(diversity_euclid, method="single")
plot(tree_euclid_single)
plot(tree_euclid_single, hang=-1)

tree_euclid_ward <- hclust(diversity_euclid, method="ward.D2")
plot(tree_euclid_ward, hang=-1)

?dist


corplot(meta_diversity)






##### inverting table !####

meta_long <- meta_diversity %>%
  pivot_longer(cols=!Species, names_to = "Station_date", values_to = "relative")
View(meta_long)

meta_long <-separate(meta_long,Station_date,c("station","date"), "_")
View(meta_long)

ggplot(meta_long, 
       aes(x=date, y=relative,fill=Species)) + 
  geom_col()+
  facet_grid(.~station, scales = "free")















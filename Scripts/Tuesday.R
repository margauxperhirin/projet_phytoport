setwd("~/Desktop/M2/_UE_Projet_Roscoff/R")

## data sets

library(readr)

micro <- read_delim("Modif_sheets/micro_spelling.csv", 
                    delim = ";", escape_double = FALSE, trim_ws = TRUE)
View(micro)

meta <- read_delim("meta-sp-g.csv", delim = ";", 
                   escape_double = FALSE, trim_ws = TRUE)
View(meta)


## comparing two lists of categorical variables

mic = micro$Species
met = meta$Species

mic_g = micro$Genus
met_g = meta$Genus

mic %in% met 

intersect(mic,met)
length(intersect(mic,met))
length(mic)
setdiff(mic,met)

intersect(mic_g,met_g)
length(intersect(mic_g,met_g))
length(unique(mic_g))
length(unique(met_g))

length(unique(met))
length(unique(mic))

table(mic,)
##### Exemple internet ####
### ex
sample <- paste(rep("sample_",24) , seq(1,24) , sep="")
specie <- c(rep("dicoccoides" , 8) , rep("dicoccum" , 8) , rep("durum" , 8))
treatment <- rep(c(rep("High",4 ) , rep("Low",4)),3)
data <- data.frame(sample,specie,treatment)
View(data)
for (i in seq(1:5)){
  gene=sample(c(1:40) , 24 )
  data=cbind(data , gene)
  colnames(data)[ncol(data)]=paste("gene_",i,sep="")
}
data[data$treatment=="High" , c(4:8)]=data[data$treatment=="High" , c(4:8)]+100
data[data$specie=="durum" , c(4:8)]=data[data$specie=="durum" , c(4:8)]-30
rownames(data) <- data[,1] 



frm <- ~SuperFamily/Family/Genus/Species
tr <- as.phylo(frm, data = carnivora, collapse=FALSE)
tr$edge.length <- rep(1, nrow(tr$edge))
plot(tr, show.node.label=TRUE, cex=0.5)
Nnode(tr)

## compare with:
Nnode(as.phylo(frm, data = carnivora, collapse = FALSE))


df <- data.frame(micro = c(rep(1, 4), rep(0, 4), rep(1, 2)), metab = c(rep(0, 4), rep(1, 4), rep(1, 2)), both = c(rep(0, 8), rep(1, 2)))

hc1 <- hclust(dist(df))

plot(hc1)

## with our data

frm <- ~SuperFamily/Family/Genus/Species
tr <- as.phylo(frm, data = carnivora, collapse=FALSE)
tr$edge.length <- rep(1, nrow(tr$edge))
plot(tr, show.node.label=TRUE, cex=0.5)
Nnode(tr)



##### diversity ####

setwd("~/Desktop/M2/_UE_Projet_Roscoff/R/Modif_sheets")


library(readxl)
meta_diversity <- read_excel("meta_diversity.xlsx", 
                             col_types = c("skip", "skip", "skip", 
                                           "skip", "text", "skip", "numeric", 
                                           "numeric", "numeric", "numeric", 
                                           "numeric", "numeric", "numeric", 
                                           "numeric", "numeric", "numeric", 
                                           "numeric", "numeric", "numeric", 
                                           "numeric", "numeric", "numeric", 
                                           "numeric", "numeric", "numeric", 
                                           "numeric", "numeric", "numeric", 
                                           "numeric", "numeric", "numeric", 
                                           "numeric"))
View(meta_diversity)

## remove the species names for diversity

meta_shannon <- meta_diversity[,-1]

## write data frame shan with shannon idex per station/date
shan <-as.data.frame(diversity(meta_shannon, index = "shannon", MARGIN = 2, base = exp(1)))
colnames(shan)<- "shannon"
## add eveness
shan$eveness <-shan$shannon/log2(specnumber(meta_shannon, MARGIN = 2))
#and richness
shan$richness <- specnumber(meta_shannon, MARGIN=2)
# add rownames to dataframe
colnames(shan)<- c("shannon","richness")

shan$stations <- row.names(shan)

# separate to get station and date
shan <-separate(shan,stations,c("station","date"), "_")
View(shan)


## plotting with ggplot

A <-ggplot(shan, aes(date, eveness, color = station, group=station)) +
  geom_point(aes(size =2))+
  geom_line()+
  labs(x="Date", y="Pielou's eveness index", color="Stations")+
  theme(axis.text=element_text(size=14),
        axis.title=element_text(size=14,face="bold"),
        legend.text=element_text(size=12),
        legend.key.size = unit(2,"line"),
        axis.text.x = element_text(angle = 45, vjust = 0.5, hjust=1))+
  ggtitle("Evenness")

B <-ggplot(shan, aes(date, shannon, color = station, group=station)) +
  geom_point(aes(size =2))+
  geom_line()+
  labs(x="Date", y="Pielou's eveness index", color="Stations")+
  theme(axis.text=element_text(size=14),
        axis.title=element_text(size=14,face="bold"),
        legend.text=element_text(size=12),
        legend.key.size = unit(2,"line"),
        axis.text.x = element_text(angle = 45, vjust = 0.5, hjust=1))+
  ggtitle("Shannon")


C <-ggplot(shan, aes(date, richness, color = station, group=station)) +
  geom_point(aes(size =2))+
  geom_line()+
  labs(x="Date", y="Pielou's eveness index", color="Stations")+
  theme(axis.text=element_text(size=14),
        axis.title=element_text(size=14,face="bold"),
        legend.text=element_text(size=12),
        legend.key.size = unit(2,"line"),
        axis.text.x = element_text(angle = 45, vjust = 0.5, hjust=1))+
  ggtitle("Species richness")


pdf(file = "~/Desktop/M2/_UE_Projet_Roscoff/figures/diversity",   
    width = 20, 
    height = 15) 

grid.arrange(A, B,C, nrow=2)

dev.off()
### making dates more agreeable

shan$month <-rep(c("march_19","april_19","may_19","june_19","july_19","august_19","sept_19","oct_19","nov_19","dec_19","jan_20","feb_20","march_20"),2)
month_order <-c("march_19","april_19","may_19","june_19","july_19","august_19","sept_19","oct_19","nov_19","dec_19","jan_20","feb_20","march_20")


A2 <-ggplot(shan, aes(factor(month, level = month_order), eveness, color = station,group=station)) +
  geom_point(aes(size =2))+
  geom_line()+
  labs(x="Date", y="Pielou's eveness index", color="Stations")+
  guides(size="none", color="none")+
  theme(axis.text=element_text(size=14),
        axis.title=element_text(size=14,face="bold"),
        title=element_text(size=16,face="bold"),
        legend.text=element_text(size=12),
        legend.key.size = unit(2,"line"),
        axis.text.x = element_text(angle = 45, vjust = 0.8, hjust=1),
        plot.title = element_text(vjust = - 10, hjust=0.01))+
  ggtitle("A")

B2 <-ggplot(shan, aes(factor(month, level = month_order), shannon, color = station,group=station)) +
  geom_point(aes(size =2))+
  geom_line()+
  labs(x="Date", y="Shannon's diversity index", color="Stations")+
  guides(size="none", color="none")+
  theme(axis.text=element_text(size=14),
        axis.title=element_text(size=14,face="bold"),
        title=element_text(size=16,face="bold"),
        legend.text=element_text(size=12),
        legend.key.size = unit(2,"line"),
        axis.text.x = element_text(angle = 45, vjust = 0.8, hjust=1),
        plot.title = element_text(vjust = - 10, hjust=0.01))+
  ggtitle("B")

B2

C2 <-ggplot(shan, aes(factor(month, level = month_order), richness, color = station,group=station)) +
  geom_point(aes(size =2))+
  geom_line()+
  labs(x="Date", y="Number of species", color="Stations")+
  guides(size="none")+
  theme(axis.text=element_text(size=14),
        axis.title=element_text(size=14,face="bold"),
        title=element_text(size=16,face="bold"),
        legend.text=element_text(size=12),
        legend.key.size = unit(2,"line"),
        axis.text.x = element_text(angle = 45, vjust = 0.8, hjust=1),
        plot.title = element_text(vjust = - 10, hjust=0.01), 
        legend.position = c(0.8, 0.2))+
  ggtitle("C")

C2

pdf(file = "~/Desktop/M2/_UE_Projet_Roscoff/figures/diversity_month_2",   
    width = 30, 
    height = 7) 

grid.arrange(A2, B2,C2, nrow=1)

dev.off()

#### beta diversity ####

## mise en forme de dataset
beta <- data.frame(t(meta_diversity[-1]))
colnames(beta) <- unique(meta_diversity$Species)


## convert into binary data frame presence-absence !
beta_bi <- beta
beta_bi[beta_bi > 0] <- 1
View(beta_bi)

## with betapart package ?

beta_div <-betapart.core(beta_bi)
View(beta_div)


## pairwise with rdist ?
B8_beta <- beta[1:13,]
View(B8_beta)

H42_beta <- beta[14:26,]
View(H42_beta)

eu_bet <-rdist(B8_beta, H42_beta)


## distance euclideen

diversity_euclid <- dist(beta, method="euclidian")
diversity_euclid

### with vegan 

ncol(meta_shannon)/mean(specnumber(meta_shannon)) - 1




### nmds --> code de AMEM

beta_Dist <- vegdist(beta, method="bray")
beta_Dist
# compute NMDS
beta_NMDS <- metaMDS(beta_Dist, k=2) # keep 3 dimensions
plot(beta_NMDS)
str(beta_NMDS)

## script de seb

data_scoresBC=as.data.frame(scores(beta_NMDS))
data_scoresBC$date <- rownames(beta)

ggplot(data_scoresBC,aes(x=NMDS1,NMDS2,label=date))+
  geom_point()+
  geom_text_repel(label=data_scoresBC$date)+
  xlab("nMDS1")+
  ylab("nMDS2")+
  labs(title="Bray-Curtis coefficient; Stress = ")





#### script from AMEM ####
# define color based on gender
library("stringr")
station <- str_sub(row.names(beta), 1,1)  # takes the first letter
cols <- ifelse(station == "B", "#2E3B99", "#AA3D63")

# plot those
library("rgl")
# prepare window
plot3d(coords, type="n")
# add text
text3d(coords, texts=row.names(coords), col=cols)
par3d(windowRect=c(20,20,600,600))



## clustering ####

tree_euclid_single <- hclust(diversity_euclid, method="single")
plot(tree_euclid_single)
plot(tree_euclid_single, hang=-1)

tree_euclid_ward <- hclust(diversity_euclid, method="ward.D2")
plot(tree_euclid_ward, hang=-1)

?dist


corplot(meta_diversity)






##### inverting table !####

meta_long <- meta_diversity %>%
  pivot_longer(cols=!Species, names_to = "Station_date", values_to = "relative")
View(meta_long)

meta_long <-separate(meta_long,Station_date,c("station","date"), "_")
View(meta_long)

ggplot(meta_long, 
       aes(x=date, y=relative,fill=Species)) + 
  geom_col()+
  facet_grid(.~station, scales = "free")














